## Preconditions

**You should install:**

**[GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)**  
**[Node.js and NPM](https://nodejs.org/en/download/)**  

*node v8.x*  
*npm v5.x*

---

## How to install?

It's nothing hard. Just follow these steps:

1. Create new foler.
2. In this folder, open **GIT** console.
3. Clone the repository by command: **git clone https://jakublipiec@bitbucket.org/jakublipiec/focus-telecom-backend.git**
4. Go to the downloaded folder by command: **cd focus-telecom-backend**
5. Install modules: **npm install**

---

## How to run?

Do the following:

1. Edit **app.js** file:
	- open the app.js file in your favorite text editor
	- enter your credentials in configuration (url, login, password)
	- enter your phone number in 'userPhoneNumber' constant
	- save file
2. Run **app.js** file:
	- node app.js
	
	or (recomended)
	
	- npm install -g nodemon
	- nodemon app.js