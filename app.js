const Dialer = require('dialer').Dialer;
const cors = require("cors");
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const port = 3000;

const server = app.listen(port, () => {
    console.log(`Application listening on port ${port}!`);
});
const io = require('socket.io')(server)

const userPhoneNumber = '<user-phone-number>';
const config = {
    url: '<url>',
    login: '<login>',
    password: '<password>'
}

Dialer.configure(config)

app.use(cors())
app.use(bodyParser.text())
app.use(bodyParser.json())

io.on('connection', (socket) => {
    socket.userName = "";
    console.log('a user connected');

    socket.on('disconnect', () => {
        console.log('a user disconnected');
    });

    socket.on('message', (msg) => {
        io.emit('message', {'user': socket.userName, 'message': msg});
    });

    socket.on('join', (user) => {
        socket.userName = user.userName;
        socket.broadcast.emit('message', {'user': 'INFO', 'message': `Dołączył użytkownik ${socket.userName}!`})
    })
});

let bridges = [];
let callsId = 0;
let endStatuses = new Set(["ANSWERED", "FAILED", "BUSY", "NO ANSWER"]);
let currentStatus;

app.post('/call', async (req, res) => {
    const body = req.body;
    callsId++;
    
    try {
        bridges[callsId] = await Dialer.call(userPhoneNumber, body.number);
    } catch (error) {
        res.json({
            success: false,
            error: error
        })
        return;
    }

    let interval = setInterval(async () => {
        let status;
        try {
            status = await bridges[callsId].getStatus();
            console.log({ status })
        } catch (error) {
            status = 'FAILED';
        }
        if (currentStatus !== status) {
            currentStatus = status;
            io.emit('status', status)
            console.log({ status })
        }
        if (endStatuses.has(currentStatus)) {
            console.log('stop')
            clearInterval(interval)
        }
    }, 1000)

    res.json({ 
        id: callsId, 
        status: bridges[callsId].STATUSES.NEW,
        success: true
    });
});

app.get('/status/:callsId', async (req, res) => {
    let callsId = req.params.callsId;
    let status;
    if (callsId in bridges) {
        try {
            status = await bridges[callsId].getStatus();
            res.json({
                success: true,
                status: status
            });
        } catch (error) {
            res.json({
                success: false,
                error: error
            });
            return;
        }
    } else {
        res.json({
            success: true,
            status: "Wrong calls id"
        });
    }
});